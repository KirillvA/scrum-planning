import React, { useState } from 'react';
import './App.css';
import { socketInit, addHandler, sendMessage } from './utils/socket';

const App = () => {
  const [currentTask, setCurrentTask] = useState({title: 'Пустой заголовок', description: 'Пустое описание', id: '1'});
  socketInit();
  addHandler({
    name: "newtaskloaded",
    callback: res => {
      setCurrentTask(res);
    }
  });
  const onTaskSizeApply = ({target}) => {  
    sendMessage(JSON.stringify({[currentTask.id]: target.getAttribute("value")}));
  }
  const taskSizes = [1, 2, 3, 5, 7];
  return (
    <div className="App">
      <div className="Title">
        {currentTask.title}
      </div>
      <div className="Description">
        {currentTask.description}
      </div>
      <div className="ButtonsWrapper">
        {taskSizes.map((size, i)=>{
          return <button key={size} className="Button" value={size} onClick={onTaskSizeApply}>{size}</button>
        })}
      </div>
    </div>
  );
}

export default App;
