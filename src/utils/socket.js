import io from "socket.io-client";
import { getConfig, GetGUID } from "./helpers";

const { WS_URL, WS_PATH } = getConfig();

let socket = null;
let handlers = [];
let isRegistry = false;

const subscribe = handler => {
  socket.on(handler.name, res => handler.callback(res));
};

const subscrideHandlers = () => {
  handlers.forEach(handler => {
    if (!handler.isSubscribe) {
      subscribe(handler);
      handler.isSubscribe = true;
    }
  });
};

const unsubscribe = handlerName => {
  socket.off(handlerName);
};

const unsubscrideHandlers = () => {
  handlers.forEach(handler => {
    if (handler.isSubscribe) {
      unsubscribe(handler.name);
      handler.isSubscribe = false;
    }
  });
};

export const socketInit = () => {
  if (socket) return;

  socket = io.connect(WS_URL, {
    path: WS_PATH,
    transports: ["websocket"],
    query: {
        id: GetGUID()
    }
  });
  socket.on("connect", () => {
    socket.on("registry", () => {
      isRegistry = true;
      subscrideHandlers();
    });
  });
  socket.on("disconnect", () => {
    isRegistry = false;
    unsubscrideHandlers();
  });
  socket.on("reconnect", () => {
    socket.on("registry", () => {
      isRegistry = true;
      subscrideHandlers();
    });
  });
};

export const addHandler = handler => {
  for (let i = 0; i < handlers.length; i++)
    if (handlers[i].name === handler.name) return;
  isRegistry && subscribe(handler);
  handlers.push({ isSubscribe: isRegistry, ...handler });
};

export const removeHandler = handlerName => {
  let newHandlers = [];
  for (let i = 0; i < handlers.length; i++)
    if (handlers[i].name !== handlerName) newHandlers.push(handlers[i]);
    else unsubscribe(handlerName);
  handlers = newHandlers;
};


export const sendMessage = message => {
  socket.send(message);
}