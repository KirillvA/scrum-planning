import data from "../config/config.json";

export const WARNING_LOG = "warning";
export const ACTION_LOG = "action";

/**
 * Возвращает уникальный идентификатор
 */
export const GetGUID = function() {
  var d = new Date().getTime();
  var uuid = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(
    c
  ) {
    var r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c === "x" ? r : r && 0x7 | 0x8).toString(16);
  });
  return uuid;
};

export const getConfig = function() {
  let config = localStorage.getItem("config");
  try {
    config = JSON.parse(config);
  } catch (error) {
    config = {};
  }
  return Object.assign(data, config);
};

export const setConfig = function(config) {
  try {
    config = JSON.stringify(config);
  } catch (error) {
   config = "{}";
  }
  localStorage.setItem("config", config);
};